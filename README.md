# OpenML dataset: wind_correlations

https://www.openml.org/d/493

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

These data are estimated correlations between daily 3 p.m. wind
measurements during September and October 1997 for a network
of 45 stations in the Sydney region.
The first column below gives a list of station latitudes,
the second gives a list of station longitudes, and
the remaining 45 columns give the 45 x 45 spatial correlation
matrix of the station measurements.
Further details about the data are contained
in the following technical report:
Nott and Dunsmuir (1998) ``Analysis of Spatial Covariance
Structure from Monitoring Data,'' Technical Report,
Department of Statistics, University of New South Wales.
Email djn@maths.unsw.edu.au with any questions or to
obtain a copy of the latest version of the above report.


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/493) of an [OpenML dataset](https://www.openml.org/d/493). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/493/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/493/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/493/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

